--
--   Copyright 2022 IIT Bombay
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
-- Script to load anthrokrishi.farmplots_osmanabad table using GeoJSON
-- data provided by Google.  The GeoJSON data may contain a mix of
-- geometries (linestrings and polygons).  Geometries other than
-- polygons are filtered out.
--
-- Existing records, if any, in farmplots_osmanabad table remain
-- unchanged.
--
-- Note: the use of "\copy" instead of the server side "copy" command
-- makes this script specific to "psql", the default client program
-- used to connect to PostgreSQL server.

begin;

\timing on

create schema if not exists anthrokrishi;

-- Staging table to load the GeoJSON data as is from csv file.  The
-- csv data is loaded as is, without any conversion, into this table.
-- Once the data is brought into PostgreSQL realm, cool stuff from
-- PostGIS can be used to filter out and convert geometries as
-- desired.
drop table if exists anthrokrishi.farmplots_geojson;
create table anthrokrishi.farmplots_geojson(
    sindex text,
    confidence numeric,
    name text,
    geom jsonb);

-- Substitute path to the GeoJSON csv file on your file system.  Note
-- the backward slash "\" at the beginning of "copy".  This triggers
-- the client side copy, avoiding cases when the server process
-- doesn't have access to the file system where the GeoJSON file is
-- located.
\copy anthrokrishi.farmplots_geojson from '{path_to_geojson_csv}' with (format csv, header true);

-- The main table that holds the farmplots in PostGIS geometry format.
create table if not exists anthrokrishi.farmplots_osmanabad(
    sindex text,
    confidence numeric,
    name text,
    geom geometry('POLYGON', 4326));

-- Load the main table with data from the staging table, converting
-- the GeoJSON values to POLYGON geometries.  The GeoJSON data may
-- contain several types of geometries, such as polygons, linestrings,
-- geometry collections, etc.  We choose only polygons, being careful
-- to expand geometry-collection and multipolygon values into
-- constituent geometries.
insert into
    anthrokrishi.farmplots_osmanabad
    select
        foo.sindex,
        foo.confidence,
        foo.name,
        foo.geom
    from
    -- Convert all GeoJSON values into Geometry objects.  This is done
    -- by dividing GeoJSON values into two groups - GeometryCollection
    -- values and simple geometry values.
    (
        -- GeometryCollection values are expanded into constinuent
        -- geometries using ST_Dump().
        select
            sindex, confidence, name,
            (ST_Dump(ST_GeomFromGeoJSON(geom))).geom as geom
        from
            anthrokrishi.farmplots_geojson
        where
            ST_GeometryType(ST_GeomFromGeoJSON(geom)) in (
                'ST_GeometryCollection',
                'ST_MultiPolygon')
        union all
        -- Simple geometries don't need any expansion.
        select
            sindex, confidence, name,
            ST_GeomFromGeoJSON(geom) as geom
        from
            anthrokrishi.farmplots_geojson
        where
            ST_GeometryType(ST_GeomFromGeoJSON(geom)) not in (
                'ST_GeometryCollection',
                'ST_MultiPolygon')
    ) foo
    where
        -- We are only interested in polygons, discard everything else
        -- (e.g. linestrings).
        lower(ST_GeometryType(foo.geom)) like '%polygon';

commit;
